package com.deshaw.structuredinterviewingjpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.deshaw.structuredinterviewingjpa.entity.Course;
import com.deshaw.structuredinterviewingjpa.repository.CourseRepository;
import com.deshaw.structuredinterviewingjpa.repository.StudentRepository;

@SpringBootApplication
public class StructuredInterviewingJpaApplication implements CommandLineRunner {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CourseRepository courseRepository;
	
	@Autowired
	private StudentRepository studentRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(StructuredInterviewingJpaApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		/*Course course = courseRepository.findById(1001L);
		logger.info("Course 1001 -> {}", course);
		courseRepository.save(new Course("JPA Advance"));*/
		
		studentRepository.saveWithPassport();
		
	}
}
