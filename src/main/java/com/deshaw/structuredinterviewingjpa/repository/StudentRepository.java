package com.deshaw.structuredinterviewingjpa.repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.deshaw.structuredinterviewingjpa.entity.Passport;
import com.deshaw.structuredinterviewingjpa.entity.Student;

@Repository
@Transactional
public class StudentRepository {

	@Autowired
	EntityManager em;

	public Student findById(Long id) {
		return em.find(Student.class, id);
	}
	
	// insert or update
	public Student save(Student student) {
		if(student.getId() == null) {
			// insert
			em.persist(student);
		} else {
			// update
			em.merge(student);
		}
		return student;
	}

	public void deleteById(Long id) {
		Student student = findById(id);
		em.remove(student);
	}
	
	// insert or update with passport
	public void saveWithPassport() {
		
		Passport passport = new Passport("ASXPA");
		em.persist(passport);
		
		Student student =  new Student("Lokesh");
		student.setPassport(passport);
		em.persist(student);
	}
}
