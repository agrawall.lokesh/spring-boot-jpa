package com.deshaw.structuredinterviewingjpa.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.deshaw.structuredinterviewingjpa.StructuredInterviewingJpaApplication;
import com.deshaw.structuredinterviewingjpa.entity.Course;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=StructuredInterviewingJpaApplication.class)
public class CourseRepositoryTest {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	CourseRepository courseRepository;
	
	@Test
	public void findById() {
		Course course = courseRepository.findById(1001L);
		assertEquals("JPA", course.getName());
	}

	@Test
	@DirtiesContext
	public void deleteById() {
		courseRepository.deleteById(1001L);
		assertNull(courseRepository.findById(1001L));
	}
	
	@Test
	@DirtiesContext
	public void save() {
		// Get the course
		Course course = courseRepository.findById(1001L);
		assertEquals("JPA", course.getName());
		
		// Update Details
		course.setName("JPA Advance");
		
		// Save 
		courseRepository.save(course);
		
		// Assert whether updated
		Course course1 = courseRepository.findById(1001L);
		assertEquals("JPA Advance", course1.getName());
	}
}
