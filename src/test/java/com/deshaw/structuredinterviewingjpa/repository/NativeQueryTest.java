package com.deshaw.structuredinterviewingjpa.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.deshaw.structuredinterviewingjpa.StructuredInterviewingJpaApplication;
import com.deshaw.structuredinterviewingjpa.entity.Course;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=StructuredInterviewingJpaApplication.class)
public class NativeQueryTest {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	EntityManager em;
	
	@Test
	public void native_query_basic() {
		Query query = em.createNativeQuery("SELECT * FROM COURSE", Course.class);
		List resultList = query.getResultList();
		logger.info("Select * from Course -> {}", resultList);
	}
	
	@Test
	public void native_query_with_parameter() {
		Query query = em.createNativeQuery("SELECT * FROM COURSE where id= ?", Course.class);
		query.setParameter(1, 1001);
		List resultList = query.getResultList();
		logger.info("Select * from Course where id = ? -> {}", resultList);
	}
	
	@Test
	public void native_query_with_named_parameter() {
		Query query = em.createNativeQuery("SELECT * FROM COURSE where id= :id", Course.class);
		query.setParameter("id", 1001);
		List resultList = query.getResultList();
		logger.info("Select * from Course where id = ? -> {}", resultList);
	}
	
	@Test
	@Transactional
	public void native_query_to_update() {
		Query query = em.createNativeQuery("UPDATE COURSE set name='JPA' ", Course.class);
		int noOfRowsUpdated = query.executeUpdate();
		logger.info("Records Updated -> {}", noOfRowsUpdated);
	}
	
}
