package com.deshaw.structuredinterviewingjpa.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.deshaw.structuredinterviewingjpa.StructuredInterviewingJpaApplication;
import com.deshaw.structuredinterviewingjpa.entity.Course;
import com.deshaw.structuredinterviewingjpa.entity.Passport;
import com.deshaw.structuredinterviewingjpa.entity.Student;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=StructuredInterviewingJpaApplication.class)
public class StudentRepositoryTest {
private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	StudentRepository studentRepository;
	
	@Autowired
	EntityManager em;
	
	@Test
	@Transactional
	public void retreiveStudentAndPassportDetails() {
		Student student = em.find(Student.class, 20001L);
		logger.info("Student -> {}", student);
		logger.info("passport details are ", student.getPassport());
	}
	
	@Test
	public void retreivePassportAndAssociatedStudentDetails() {
		Passport passport = em.find(Passport.class, 1L);
		logger.info("Passport -> {}", passport);
		logger.info("Student details are ", passport.getStudent());
	}

	
}
